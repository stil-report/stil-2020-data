# A platform survey: interface language support by widely-used websites and mobile apps

By Martin Dittus (@dekstop) and Mark Graham (@geoplace), Oxford Internet Institute, University of Oxford, in 2020. With generous support by Whose Knowledge?

## About this data release

This is a companion release for our report "State of the Internet's Languages", a collaborative effort by Whose Knowledge?, the Centre for Internet & Society, and the Oxford Internet Institute.

The report includes further details about our data collection process, and a detailed discussion of the results.

An open access version of the report is available at: http://internetlanguages.org

## Description of the data

The data is provided in CSV format. It records a list of interface languages supported by each platform included in our platform survey. 

For each platform and language we capture: 

- The original language name and code as stated by the respective platform. These are specific to the respective platform. Depending on the platform, language codes may be recorded in a standard form such as an ISO 639 language code or RFC 5646 language tag, or not recorded at all.
- Whenever a language code was not provided by the platform we derive an ISO 639 language code from the language name.
- Based on the above we derive a single standardised language code and name. The standardised language code is comparable across the surveyed platforms and can be used for analysis, its associated language name is provided for convenience. (We derive these with the method "langcodes.standardize_tag" offered by the Python langcodes package.)

The CSV file includes the following columns:

- Type - The type of platform: Website, iOS App, or Android App.
- Name - Name of the platform.
- OriginalLanguageCode - The language identifier code as stated by the platform, if specified (optional).
- OriginalLanguageName - The language name as stated by the platform, if specified (optional).
- OriginalLanguageLocalName - The language name expressed in its own language and script, if specified (optional).
- DerivedLanguageCode - A language code derived from the language name, if a language identifier was not provided.
- LanguageCode - A standardised language code that can be used for analysis.
- LanguageName - The associated language name.

The fields Type, Name, LanguageCode and LanguageName are the primary data points that can be used for analysis.

The fields OriginalLanguageCode, OriginalLanguageName, OriginalLanguageLocalName, and DerivedLanguageCode are included to offer visibility into our data collection process. They can be used to verify the impact of any of our standardisation steps.

## Licence

This data is made available under the Open Database License 1.0 (Attribution and Share-Alike). A copy of the licence is included in the file: odbl-10.txt

A human-readable summary of the terms of the licence is available at: https://opendatacommons.org/licenses/odbl/summary/

Any rights in individual contents of the database are licensed under the Database Contents License: http://opendatacommons.org/licenses/dbcl/1.0/

## Contact

For questions about the data, please contact: martin@dekstop.de

For further information about our work and our research, visit:

- https://whoseknowledge.org
- https://cis-india.org
- https://geography.oii.ox.ac.uk
